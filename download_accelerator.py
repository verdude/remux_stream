#!/usr/bin/env python

import threading
import sys, logging
import argparse
import requests
from urllib.parse import urlparse
import time

class Dl_thread(threading.Thread):
    """ The worker Thread functionality """
    def __init__(self,url):
        threading.Thread.__init__(self)
        self.url = url

    def run(self):
        """ Download the range that is specified """
        self.get_content(self.gen_headers())

    def get_content(self, headers):
        """ Makes the GET request to get the range of content that was specified """
        r = requests.get(self.url)

        if not r.ok:
            logging.error("Request for %s failed." % self.url)
            self.content = None
        else:
            self.content = r.content

    def gen_headers(self):
        headers = {}
        headers["Accept-Encoding"] = "identity"
        headers["Range"] = "bytes=%d-%d" % self.range
        logging.debug("headers: %s" % str(headers))
        return headers

class Download_Manager():
    """ handles the threading and organization of a download """
    def __init__(self,filename,num_threads,urls):
        if num_threads > len(urls):
            logging.error("Cannot have more threads than urls.")
            return

        if len(urls) != num_threads:
            extra = num_threads - urls
            if extra % num_threads == 0:
                print("hi")
        self._threads = []
        self._urls = urls
        self._filename = filename
        self._num_threads = num_threads
        self._end_time = 0.0
        self._start_time = 0.0
        self._dl_success = False


    def real_url(self):
        url = urlparse(self._urls)
        if url.scheme is "" or url.hostname is "":
            logging.error("Url is bad: %s" % self._urls)
            return False
        else:
            logging.debug("Url is good: %s" % self._urls)
            return True

    def manage(self):
        """ The main logic for the downloader """
        if not self.real_url():
            logging.error("Bad URl. Exiting Download_Manager.manage.")
            return
        self.make_threads()
        self.download()
        self.write_to_file()
            
    def make_threads(self):
        """ Makes the threads for the download """
        self._start_time = float(time.time())*1000
        for x in range(0, self._num_threads):
            self._threads.append(Dl_thread(url))

    def download(self):
        """ starts all of the threads """
        for thread in self._threads:
            thread.start()
        for thread in self._threads:
            thread.join()
        self._end_time = float(time.time())*1000

    def write_to_file(self):
        """ Puts the downloaded content back together again """
        logging.info("writing to %s" % self._filename)
        with open(self._filename, "w") as dl_file:
            for thread in self._threads:
                if thread.content is None:
                    logging.error("Download failed.")
                    return
                else:
                    dl_file.write(thread.content)
        self._dl_success = True

    def get_stats(self):
        if not self._dl_success:
            logging.error("Download Failed")
            return "Download Failed"
        stats = "[%s] [%s] [%f]" % (self._urls, self._num_threads, (self._end_time-self._start_time)/1000)
        return stats

def parse_options():
    parser = argparse.ArgumentParser(prog="Download Accelerator", description="Threaded Static File Downloader", add_help=True)
    parser.add_argument("-d", "--debug", action="store_true", help="Turn on logging")
    parser.add_argument("-n", "--number", type=int, action="store", help="Specify the number of threads to create",default=1)
    parser.add_argument("url", help="The url of the page that you want to download")
    return parser.parse_args() 

if __name__ == "__main__":
    args = parse_options()
    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)
    num_threads = args.number if args.number > 0 else 1
    dlmgr = Download_Manager(num_threads=num_threads, url=args.url)
    print(dlmgr.get_stats())


