#!/usr/bin/env python3

import requests
import argparse
import logging

import download_accelerator

class RenditionMetadata():
    def __init__(self, data):
        meta = data[0]
        bandwidth,resolution = meta.split(",")[-2:]
        self.bandwidth = bandwidth.split("=")[1].strip()
        self.resolution = resolution.split("=")[1].strip()
        self.link = data[1].strip()

    def pixels(self):
        w,h = self.resolution.split("x")
        return int(w) * int(h)

class Rendition():
    def __init__(self, num_threads, num_accs):
        self.num_threads = num_threads
        self.num_accs = num_accs
        self.accs = []
        self.urls = []

    def parse(self, data):
        def not_comment(string):
            return len(string) > 0 and string[0] != "#"

        self.urls = [line.strip() for line in data.split("\n") if not_comment(line)]

    def download_and_save(self):
        if len(self.urls) == 0:
            l("Download and parse m3u first.")
            return

        urlslen = len(self.urls)
        logging.info("Beginning .ts file download.")
        with open("tmp.ts", "w") as tsfile:
            for i in range(0, urlslen, self.num_threads):
                batch = self.urls[i:i+self.num_threads]
                
                if res.ok:
                    tsfile.write(res.text)
                logging.info("%s%% complete" % ((i+1)/urlslen * 100))

def parse_options():
    parser = argparse.ArgumentParser(prog="Stream Remux", description="Brightcove HLS Remuxer", add_help=True)
    parser.add_argument("-d", "--debug", action="store_true", help="Turn on logging")
    parser.add_argument("-n", "--number", type=int, action="store", help="Specify the number of threads to create",default=1)
    parser.add_argument("-a", "--accelerators", type=int, action="store", help="Specify the number of accelerators to create",default=1)
    parser.add_argument("-f", "--file", help="The .m3u file", required=True)
    return parser.parse_args() 
    
if __name__ == "__main__":
    args = parse_options()
    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)
    with open(args.file, "r") as master:
        lines = master.readlines()[1:]
        versions = []

        for pair in [lines[i:i+2] for i in range(0, len(lines), 2)]:
            versions.append(RenditionMetadata(pair))

        bestQuality = max(versions, key=RenditionMetadata.pixels)
        logging.info("%s is the highest quality version." % bestQuality.resolution)
        metadata = bestQuality

    response = requests.get(metadata.link)
    if not response.ok:
        logging.error("Error getting rendition m3u file.")
    else:
        rendition = Rendition(args.number, args.accelerators)
        rendition.parse(response.text)
        rendition.download_and_save()

